const newSpellCasts = {
  "SpellcastsByPowerLevel": [
    {"Values": ["2", "0", "0", "0", "0", "0", "0", "0", "0"]},
    {"Values": ["2", "1", "0", "0", "0", "0", "0", "0", "0"]},
    {"Values": ["2", "1", "1", "0", "0", "0", "0", "0", "0"]},
    {"Values": ["2", "2", "1", "1", "0", "0", "0", "0", "0"]}, // { "Values": [ "2", "2", "2", "1", "0", "0", "0", "0", "0" ] },
    {"Values": ["3", "2", "2", "1", "1", "0", "0", "0", "0"]}, // { "Values": [ "2", "2", "2", "2", "1", "0", "0", "0", "0" ] },
    {"Values": ["3", "3", "2", "2", "1", "1", "0", "0", "0"]}, // { "Values": [ "2", "2", "2", "2", "2", "1", "0", "0", "0" ] },
    {"Values": ["-1", "3", "3", "2", "2", "1", "1", "0", "0"]}, // { "Values": [ "2", "2", "2", "2", "2", "2", "1", "0", "0" ] },
    {"Values": ["-1", "-1", "3", "3", "2", "2", "1", "1", "0"]}, // { "Values": [ "2", "2", "2", "2", "2", "2", "2", "1", "0" ] },
    {"Values": ["-1", "-1", "-1", "3", "3", "2", "2", "1", "1"]}, // { "Values": [ "2", "2", "2", "2", "2", "2", "2", "2", "1" ] },
  ]
};

const config = {
  characterPlainLevel: {
    def: 3,
    rules: [
      [/dragon|kraken|eothas|Beast_of_Winter/i, 11],
      [/titan|Tyrant_of_Decay/, 9],
      [/bear|giant|eoten|ogre|Vathor_Cadhu/i, 7],
      [/beetle|bird|bat/i, 1],
    ]
  },
  characterStatsOverride: {
    "BasePlayerLevelCap": 23, // 20
    "AttributeHealthMultiplier": 0.05,
    "AttributeDefenseAdjustment": 2,
    "AttributeDeflectionAdjustment": 1,
    "AttributeAccuracyAdjustment": 1,
    "AttributeDamageMultiplier": 0.03,
    "AttributeAttackSpeedMultiplier": 0.03,
    "AttributeDurationMultiplier": 0.05, // 0.05
    "AttributeIncomingHostileEffectDuationMultiplier": -0.03,
    "AttributeTacticalStrideMultiplier": 0.04,
    "AttributeAreaMultiplier": 0.1,
    "IngestibleDurationMultiplier": 0,
    "MaxAttributeScore": 35,
    "PersonalInventorySize": 14,
    "BaseQuickbarSize": 6, // 4
    "BaseWeaponSets": 2,
    "EnemyDifficultyAdjustments": [
      {
        "Difficulty": "Easy",
        "HealthMultiplier": 0.5,
        "AccuracyBonus": 0,
        "DefenseBonus": 0,
        "ArmorBonus": 0,
        "PenetrationBonus": 0,
        "LevelMultiplier": 1,
        "RecoveryTimeMultiplier": 1,
        "DisengagementAccuracyBonus": 0,
        "HostileEffectDurationMultiplier": 1
      },
      {
        "Difficulty": "Normal",
        "HealthMultiplier": 1,
        "AccuracyBonus": 0,
        "DefenseBonus": 0,
        "ArmorBonus": 0,
        "PenetrationBonus": 0,
        "LevelMultiplier": 1,
        "RecoveryTimeMultiplier": 1,
        "DisengagementAccuracyBonus": 0,
        "HostileEffectDurationMultiplier": 1
      },
      {
        "Difficulty": "Hard",
        "HealthMultiplier": 1.12,
        "AccuracyBonus": 8,
        "DefenseBonus": 8,
        "ArmorBonus": 1,
        "PenetrationBonus": 1,
        "LevelMultiplier": 1,
        "RecoveryTimeMultiplier": 1,
        "DisengagementAccuracyBonus": 0,
        "HostileEffectDurationMultiplier": 1
      },
      {
        "Difficulty": "PathOfTheDamned",
        "HealthMultiplier": 1.25,
        "AccuracyBonus": 15,
        "DefenseBonus": 15,
        "ArmorBonus": 2,
        "PenetrationBonus": 2,
        "LevelMultiplier": 1,
        "RecoveryTimeMultiplier": 1,
        "DisengagementAccuracyBonus": 0,
        "HostileEffectDurationMultiplier": 1
      },
      {
        "Difficulty": "StoryTime",
        "HealthMultiplier": 0.5,
        "AccuracyBonus": 0,
        "DefenseBonus": 0,
        "ArmorBonus": 0,
        "PenetrationBonus": 0,
        "LevelMultiplier": 0.75,
        "RecoveryTimeMultiplier": 1.5,
        "DisengagementAccuracyBonus": -25,
        "HostileEffectDurationMultiplier": 0.5
      }
    ],
    "LevelScalingArmorLevelIncrement": 3,
    "LevelScalingArmorModifier": 1,
    "LevelScalingPenetrationLevelIncrement": 2,
    "LevelScalingPenetrationModifier": 1,
  },
  progressionOverwrite: {
    "MaxClasses": 6,
    "SuperChargeCountByCharacterLevel": [
      // "0", "3", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "1"
      "0", "3", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "1", "0", "0", "0"
    ],
    "WeaponProficiencyPointsByCharacterLevel": [
      // "2", "0", "0", "1", "0", "0", "0", "1", "0", "0", "0", "1", "0", "0", "0", "1", "0", "0", "0", "1"
      "2", "0", "0", "1", "0", "0", "0", "1", "0", "1", "0", "1", "0", "1", "0", "1", "1", "1", "1", "1", "1", "1", "1"
    ],
    "BaseMaxPowerPoolByPowerLevel": [
      "0", "3", "4", "5", "6", "7", "8", "9", "10", "11"
    ],
    "SingleClassPowerLevelByCharacterLevel": [
      // "1", "1", "2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "7", "8", "8", "8", "9", "9", "9", "10", "10", "10", "11", "11", "11", "12", "12", "12"
      "1", "1", "2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "7", "8", "8", "8", "9", "9", "9", "10", "10", "10", "11", "11", "11", "12", "12", "12", "12", "12", "12"
    ],
    "MultiClassPowerLevelByCharacterLevel": [
      // "1", "1", "1", "2", "2", "2", "3", "3", "3", "4", "4", "4", "5", "5", "5", "6", "6", "6", "7", "7", "7", "8", "8", "8", "9", "9", "9", "10", "10", "10"
      "1", "1", "2", "2", "3", "3", "4", "4", "5", "5", "6", "6", "7", "7", "7", "8", "8", "8", "9", "9", "9", "10", "10", "10", "11", "11", "11", "12", "12", "12", "12", "12", "12"
    ]
  },
  characterClassOverwrite: {
    Druid: {...newSpellCasts},
    Priest: {...newSpellCasts},
    Wizard: {...newSpellCasts},
  }
};

module.exports = function (ctx) {
  const modKit = ctx.getModKit('Characters');

  // De-level health
  modKit._mod({component: cmp => cmp.HealthPerLevel}, {component: processCharacter});

  // Progression + 23 levels
  modKit.overwrite('CharacterProgressionGameData', 'CharacterProgressionDataComponent', config.progressionOverwrite);
  modKit.mod('SkillManagerGameData', 'SleightOfHandSettingsComponent',
      cmp => {
        cmp.LevelModifiers.push(
            {"Level": 21, "Modifier": 9},
            {"Level": 22, "Modifier": 9},
            {"Level": 23, "Modifier": 9},
        );
      });

  function generateLevels(type, componentType, fieldGetter, levels = 3) {
    modKit.mod(type, componentType, cmp => {
      const ref1 = {...fieldGetter(cmp)[fieldGetter(cmp).length - 3]};
      let refCurrent = {...fieldGetter(cmp)[fieldGetter(cmp).length - 2]};
      const refDiff = {...refCurrent};
      for (let k in refDiff) {
        refDiff[k] -= ref1[k];
      }

      function getNext() {
        refCurrent = {...refCurrent};
        for (let k in refDiff) {
          refCurrent[k] += refDiff[k]
        }
        return refCurrent;
      }

      fieldGetter(cmp)[fieldGetter(cmp).length - 1] = getNext(); // 20
      for (let i = levels; i-- > 0;) {
        fieldGetter(cmp).push(getNext());
      } // 21, 22...
    });
  }

  generateLevels('ExperienceTableGameData', 'ExperienceTableComponent', cmp => cmp.ExperienceReference);
  for (let i = 0; i < 3; i++) {
    generateLevels('ExperienceTableGameData', 'ExperienceTableComponent', cmp => cmp.WorldMapEncounterExperience[i].ExperienceList);
  }

  // Classes overwrite
  modKit.withDebugNames(Object.keys(config.characterClassOverwrite))
      .overwrite('CharacterClassGameData', 'CharacterClassComponent',
          (cmp, item) => Object.assign(cmp, config.characterClassOverwrite[item.DebugName]));

  // Character stats
  modKit.overwrite('GlobalGameSettingsGameData', "CharacterStatsSettingsComponent", config.characterStatsOverride);
  modKit.mod('ProgressionTableManagerGameData', 'ProgressionTableManagerComponent',
      cmp => cmp.CharacterCreationAttributePoints = 20);
};

function processCharacter(cmp, item) {
  let levelToFix = config.characterPlainLevel.def;
  for (let i = 0; i < config.characterPlainLevel.rules.length; i++) {
    let [re, level] = config.characterPlainLevel.rules[i];
    if (re.test(item.DebugName)) {
      levelToFix = level;
      break;
    }
  }
  cmp.MaxHealth += cmp.HealthPerLevel * levelToFix;
  cmp.HealthPerLevel = 0;
  return true;
}
