const config = {
  combatSettings: {
    "PenetrationRatioMultipliers": [
      {"Threshold": 0, "Value": 1},
      {"Threshold": 1, "Value": 1},
      {"Threshold": 2, "Value": 1.3}
    ],
    "FlatPenetrationMultipliers": [ // -99/0.25, -2/-.5, -1/0.75, 0/1
      {"Threshold": -99, "Value": 0.15},
      {"Threshold": -4, "Value": 0.2},
      {"Threshold": -3, "Value": 0.25},
      {"Threshold": -2, "Value": 0.5},
      {"Threshold": -1, "Value": 0.75},
      {"Threshold": 0, "Value": 1}
    ],
    "NoPenetrationNotification": 0.3,
    "GlobalRecoveryMultiplier": 2,
    "DualWeaponRecoveryMultiplier": 0.7,
    "DualWeaponFullAttackDamageMultiplier": 0.65,
    "SingleWeaponAccuracyBonus": 12,
    "SwitchWeaponRecoveryTime": 1, // 2
    "InterruptRecoveryTime": 2,
    "MovingRecoverySpeedMultiplier": 0.5,
    "ReengagementWaitPeriod": 2,
    "GrimoireCooldownTime": 2,
    "StoryTimeMinimumRollToCrit": 125,
    "MinimumRollToGraze": 25,
    "MinimumRollToHit": 50,
    "MinimumRollToCrit": 100,
    "MinimumRollToGrazeTactical": 1,
    "MinimumRollToHitTactical": 50,
    "MinimumRollToCritTactical": 100,
    "GrazeDamageMult": 0.5,
    "GrazeEffectDurationMult": 0.5,
    "CritDamageMult": 1.25,
    "CritEffectDurationMult": 1.25,
    "CritPenetrationMult": 1.5,
    "AccuracyPerLevel": 1, // 3
    "DefensePerLevel": 1, // 3
    "DisengagementAccuracyBonus": 20,
    "DisengagementDamageBonus": 0, // 2
    "TrapBaseAccuracy": 50,
    "TrapAccuracyPerLevel": 1, // 3
    "StatusEffectDefaultPenetration": 9,
    "MaximumInjuriesBeforeDeath": 3,
    "KnockDownDuration": 1,
    "PostCombatRefreshTime": 3,
    "PartyDefogRadius": 30, // 14
    "EnemySightRange": 12,
    "RangedAutoAttackDistance": 2,
    "RemainingRecoveryDodgeTimer": 1,
    "CombatMoveSpeedMultiplier": 0.85,
    "StealthLingerDuration": 2,
    "CastHeightRange": 100,
    "HighSpellShapingAreaAdjustment": 1.5,
    "LowSpellShapingAreaAdjustment": 0.5,
    "HighSpellShapingPowerLevelAdjustment": -3, // -5
    "LowSpellShapingPowerLevelAdjustment": 2, // 1
    "InvocationChantDelay": 2
  },
  abilitySettings: {
    "BasePhraseRecitation": 6,
    "BasePhraseLinger": 3,
    "ChantRadius": 4,
    "AbilityBaseAccuracyPerAbilityLevel": 1, // 2
    "AbilityBasePenetrationPerAbilityLevel": 0.25, // 0.5
    "EmpoweredWeaponAccuracyBonus": 10,
    "EmpoweredWeaponPenetrationBonus": 2.5,
    "DefaultMultiProjectileScaling": {
      "ScalingType": "Default",
      "BaseLevel": 0,
      "LevelIncrement": 1,
      "MaxLevel": 0,
      "DamageAdjustment": 1.0, // 1.05
      "DurationAdjustment": 1,
      "BounceCountAdjustment": 0,
      "ProjectileCountAdjustment": 0.5,
      "AccuracyAdjustment": 1,
      "PenetrationAdjustment": 0.25
    },
    "DefaultBounceScaling": {
      "ScalingType": "Default",
      "BaseLevel": 0,
      "LevelIncrement": 1,
      "MaxLevel": 0,
      "DamageAdjustment": 1.0, // 1.05
      "DurationAdjustment": 1,
      "BounceCountAdjustment": 0.5,
      "ProjectileCountAdjustment": 0,
      "AccuracyAdjustment": 1,
      "PenetrationAdjustment": 0.25
    },
    "DefaultEffectScaling": {
      "ScalingType": "Default",
      "BaseLevel": 0,
      "LevelIncrement": 1,
      "MaxLevel": 0,
      "DamageAdjustment": 1.05,
      "DurationAdjustment": 1.05,
      "BounceCountAdjustment": 0,
      "ProjectileCountAdjustment": 0,
      "AccuracyAdjustment": 1,
      "PenetrationAdjustment": 0.25
    },
    "DefaultWeaponAttackScaling": {
      "ScalingType": "Default",
      "BaseLevel": 0,
      "LevelIncrement": 1,
      "MaxLevel": 0,
      "DamageAdjustment": 1.05,
      "DurationAdjustment": 1.05,
      "BounceCountAdjustment": 0,
      "ProjectileCountAdjustment": 0,
      "AccuracyAdjustment": 1,
      "PenetrationAdjustment": 0.25
    },
    "DefaultFallbackScaling": {
      "ScalingType": "Default",
      "BaseLevel": 0,
      "LevelIncrement": 1,
      "MaxLevel": 0,
      "DamageAdjustment": 1.05,
      "DurationAdjustment": 1,
      "BounceCountAdjustment": 0,
      "ProjectileCountAdjustment": 0,
      "AccuracyAdjustment": 2,
      "PenetrationAdjustment": 0.25
    },
  }
};

module.exports = function (ctx) {
  const modKit = ctx.getModKit('Settings');

  modKit.overwrite('SkillManagerGameData', 'StealthSettingsComponent',
      {
        HearingSuspicionRate: 0, // 40
        VisionSuspicionRate: 200, // 80
        MinDistanceMultiplier: 4, // 2
        MaxDistanceMultiplier: 1, // 0.5
        StealthDecayRate: 150, // 15
      }
  );

  modKit.overwrite('WorldTimeSettingsGameData', "WorldTimeSettingsComponent", {
    GameToRealTimeRatio: 8, // 24
  });

  modKit.mod('NewGameBonusGroupGameData', "NewGameBonusGroupComponent", cmp => cmp.Bonuses.forEach(bonus => bonus.Cost = 1));
  modKit.overwrite('GlobalGameSettingsGameData', "CombatSettingsComponent", config.combatSettings);
  modKit.overwrite('AbilitySettingsGameData', "AbilitySettingsComponent", config.abilitySettings);

  // Timescale
  modKit.overwrite('GlobalGameSettingsGameData', "WorldMapSettings", {
    BaseSailSpeed: 10, // 5
    BaseWalkSpeed: 4, // 2
    GameSecondsPerRealSecond: 1500, // 3000
  });

  modKit.overwrite('GlobalGameSettingsGameData', 'CharacterStatsSettingsComponent', {
    MaxDetectionRange: 24, // 12
  });

  // CharacterCreationSettingsGameData
  // GlobalShipSettingsGameData
  // SpellSettingsComponent
  // AISettingsComponent
  // ShipDuelSettingsGameData
};
