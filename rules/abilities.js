const config = {
  bloodMageOverwrite: {
    Blood_Sacrifice_SE_AddResource_Tier1: {ExtraValue: 4},
    Blood_Sacrifice_SE_AddResource_Tier2: {ExtraValue: 6},
    Blood_Sacrifice_SE_AddResource_Tier3: {ExtraValue: 9},
    Blood_Sacrifice_SE_RawDamage_Tier1: {BaseValue: 5},
    Blood_Sacrifice_SE_RawDamage_Tier2: {BaseValue: 5},
    Blood_Sacrifice_SE_RawDamage_Tier3: {BaseValue: 10},
    Blood_Sacrifice_SE_PowerLevel: {BaseValue: 3},
  },
  abilitiesOverwrite: {
    Smoke_Veil: {IsCombatOnly: "false"}
  },
  shapeshiftAdditions: [
    "808e813a-876d-49d0-8141-944e1836fc56", // Fit
    "9ac0f508-7dcf-47e3-9e2d-ef6b2006968f", // Strong
    "df62a582-8539-4e38-bc62-adcb7129a2a1", // Armor
    "c15f034c-772b-472f-b6ef-43ef69956015", // Armor
    "1883e0e5-b524-4f59-b62e-d3a6c0582a91", // Armor
    "b9881854-0ad9-4eb4-bbfa-6eaaa88e6869", // Armor
  ]
};

module.exports = function (ctx) {
  const modKit = ctx.getModKit('Abilities');
  // Blood Mage
  modKit
      .withDebugNames(Object.keys(config.bloodMageOverwrite))
      .mod('StatusEffectGameData', 'StatusEffectComponent',
          (cmp, item) => Object.assign(cmp, config.bloodMageOverwrite[item.DebugName]));

  // Overwrite
  modKit
      .withDebugNames(Object.keys(config.abilitiesOverwrite))
      .mod('GenericAbilityGameData', 'GenericAbilityComponent',
          (cmp, item) => Object.assign(cmp, config.abilitiesOverwrite[item.DebugName]));

  // Shapeshift additions
  modKit.withDebugNames([
    'Spiritshift_Bear',
    'Spiritshift_Bear_Shifter',
    'Spiritshift_Boar',
    'Spiritshift_Boar_Shifter',
    'Spiritshift_Cat',
    'Spiritshift_Cat_Flurry',
    'Spiritshift_Cat_Shifter',
    'Spiritshift_Shark',
    'Spiritshift_Stag',
    'Spiritshift_Stag_Shifter',
    'Spiritshift_StormBlight',
    'Spiritshift_Wolf',
    'Spiritshift_Wolf_Shifter',
  ]).mod('GenericAbilityGameData', 'GenericAbilityComponent', cmp => {
    cmp.UsageType = "PerRest"; // PerEncounter
    cmp.DurationOverride = 180; // 22
    cmp.StatusEffectsIDs.push(...config.shapeshiftAdditions);
  });
};
