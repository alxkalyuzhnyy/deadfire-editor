const config = {
  consumableUsages: {
    Explosive: {
      UsageCount: 1,
      UsageType: 'Fixed',
    },
    Drug: {
      UsageCount: 1,
      UsageType: 'Fixed',
    },
    Potion: {
      UsageCount: 5,
      UsageType: 'Fixed',
    },
    Scroll: {
      UsageCount: 1,
      UsageType: 'PerEncounter',
    },
    Figurine: {},
  },
  consumableOverwrite: {
    L1_Potion_of_minor_healing: {
      AbilityID: "00a60d67-69d1-40b6-80e4-c2d7211f315d",
      UsageCount: 1,
    },
    L4_Potion_of_moderate_healing: {
      AbilityID: "00a60d67-69d1-40b6-80e4-c2d7211f315d",
      UsageCount: 3,
    },
    L7_Potion_of_major_healing: {
      AbilityID: "00a60d67-69d1-40b6-80e4-c2d7211f315d",
      UsageCount: 5,
    },
    L9_Potion_of_miraculous_healing: {
      AbilityID: "fde2f908-2622-41fc-9016-805e02968783",
      UsageCount: 5,
    },
    Drug_Deadeye: {UsageCount: 3},
    Drug_Coral_snuff: {UsageCount: 10},
    Drug_Mouth_Char: {UsageCount: 10},
    Drug_Taru_Turu_Chew: {UsageCount: 5},
    Unguent_Arcanists_Balm: {UsageCount: 10},
    Unguent_Blessed_incense: {UsageCount: 10},
    Unguent_salve_of_dissolution: {UsageCount: 10},
    Unguent_Thiefs_Putty: {UsageCount: 10},
    Unguent_Unguent_of_animalism: {UsageCount: 10},
    Bomb_L9_Implosion_charge: {UsageType: 'PerEncounter',},
    Bomb_L7_Blister_bomb: {UsageType: 'PerRest',},
    Bomb_L9_Frost_bomb: {UsageType: 'PerRest',},
    Bomb_L8_Lightning_bomb: {UsageType: 'PerRest',},
    LAX02_Poison_l_Essence: {UsageType: 'PerRest',}, // Corrosive soul poison
    Bomb_L1_Sparkcrackers: {
      UsageCount: 3,
      UsageType: 'PerRest',
    },
    Beraths_Black_Bell: {
      UsageCount: 1,
      UsageType: "PerRest"
    },
  }
};

module.exports = function (ctx) {
  const modKit = ctx.getModKit('Items');

  // Consumables
  const overrideConsumablesTypes = Object.keys(config.consumableUsages);
  modKit._mod({
    component: cmp =>
        cmp.$type === 'Game.GameData.ConsumableComponent, Assembly-CSharp' &&
        cmp.UsageType === "Fixed" &&
        overrideConsumablesTypes.indexOf(cmp.Type) >= 0
  }, {
    component: (cmp, item) => {
      if (item.DebugName.toLowerCase().indexOf('_summoned_') >= 0) {
        return false;
      }
      Object.assign(cmp, config.consumableUsages[cmp.Type]);
      item.Components.forEach(cmp => cmp.MaxStackSize && (cmp.MaxStackSize = 1));
      if (config.consumableOverwrite[item.DebugName]) {
        Object.assign(cmp, config.consumableOverwrite[item.DebugName]);
      }
    }
  });

  // Potions outside combat
  modKit._mod({
    type: 'GenericAbilityGameData',
    item: item => item.DebugName.indexOf('Potion_') === 0,
    component: cmp => cmp.IsCombatOnly === "true"
  }, {
    component: cmp => cmp.IsCombatOnly = "false"
  });

  // Currencies
  const currencyPrice = {
    def: 1.35,
    BUX_Obsidian_: 2.5,
    BUX_Adra_: 4,
    BUX_Golden_: 5,
  };
  modKit._mod({
    type: 'ItemGameData',
    component: cmp => cmp.IsCurrency === "true"
  }, {
    component: (cmp, item) => {
      cmp.IsCurrency = "false";
      for (let k in currencyPrice) {
        if (item.DebugName.indexOf(k) === 0) {
          return cmp.Value = Math.round(cmp.Value * currencyPrice[k]);
        }
      }
      cmp.Value = Math.round(cmp.Value * currencyPrice.def);
      return true;
    }
  });

  // Infinite Part_Mythical_Adra_Stone
  modKit.mod('RecipeData', 'RecipeComponent',
      cmp => {
        let shouldUpdate = !!cmp.Cost;
        cmp.Cost = 0;
        cmp.Ingredients.forEach(ing => {
          if (ing.ItemID === 'f51bb759-c5b3-437f-84ea-ba5671d3fa27' && ing.IsDestroyed === "true") {
            ing.IsDestroyed = "false";
            shouldUpdate = true;
          }
        });
        return shouldUpdate;
      });


  // Item enchantments Fixed to PerRest
  modKit._mod({
    type: 'GenericAbilityGameData',
    component: cmp => cmp.UsageType === "Charged"
  }, {
    component: cmp => cmp.UsageType = "PerRest"
  });
};