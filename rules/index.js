module.exports = [
    require('./settings'),
    require('./characters'),
    require('./abilities'),
    require('./items'),
];