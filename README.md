# PoE 2 Deadfire Editor
Batch edit tool for PoE 2 Deadfire. Autocreates mod with overrides over base content ( including DLCs )

Put content next to `exported` directory to your PoE 2 installation.
Run `editor.js` with Node ( eg. `node ./editor/editor.js` ) .

### Configuration:
`rules.js` file is basically a configuration.
Example:
```javascript
module.exports = function (ctx) {
  ctx.mod({component: cmp => cmp.HealthPerLevel}, {
    component: cmp => {
      cmp.MaxHealth += cmp.HealthPerLevel * 5;
      cmp.HealthPerLevel = 0;
      return true;
    }
  });
};
```
Code in this example effectively de-levels all characters in game.
Step-by-step:
1. Filter items that have components with `HealthPerLevel` field
2. Alter each component `MaxHealth` to value of 5th level
3. Alter each component  `HealthPerLevel` value to 0
4. `return true` means this component should be included in mod

**Note**: filter aplies to all records that were parsed, including characters, classes, items, ships, ai, settings, globals, etc...

#### Multiple configurations:
```javascript
module.exports = function (ctx) {
  ctx.mod({component: cmp => cmp.HealthPerLevel}, {
    component: cmp => {
      cmp.MaxHealth += cmp.HealthPerLevel * 5;
      cmp.HealthPerLevel = 0;
      return true;
    }
  });

  ctx.mod({
      component: cmp =>
          cmp.$type === 'Game.GameData.ConsumableComponent, Assembly-CSharp' &&
          cmp.Type === 'Potion'
    }, {item: item => {
      item.UsageType = "PerRest";
      return true;
    }});
};
```
This example in addition to plain leveling changes potions use type to `"PerRest"`, so potions should refill on rest.
