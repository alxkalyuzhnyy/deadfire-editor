function filter(data, filters = {}) {
  filters.item = filters.item || (() => true);
  filters.component = filters.component || (() => true);

  if ( filters.type ) {
    filters.type = formatComponentName(filters.type);
  }

  let filteredComponents = [];
  const filteredItems = data.filter(item => {
    if (filters.type && item.$type !== filters.type) {
      return;
    }
    if (!filters.item(item)) {
      return;
    }
    if (item.Components) {
      let componentsCheck = false;

      item.Components.forEach(component => {
        if (filters.component(component)) {
          filteredComponents.push({component, item});
          componentsCheck = true;
        }
      });

      return componentsCheck
    }
  });

  return {
    items: filteredItems,
    components: filteredComponents,
  }
}


function formatComponentName(key) {
  if (key.startsWith('Game.GameData.')) {
    return key;
  }
  return `Game.GameData.${key}, Assembly-CSharp`
}

module.exports = {filter, formatComponentName};
