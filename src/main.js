const path = require('path');
const fs = require('fs');
const processModule = require('./process-module');
const {filter, formatComponentName} = require('./filter');
const config = require('./config');

const rootPath = path.resolve.bind(this, __dirname, '../..');


module.exports = function main(process, rules) {
  const ctx = {
    tree: {},
    list: [],
    moddedTree: {},
    moddedList: [],
    statistics: {
      moddedByRule: {},
      moddedByType: {}
    }
  };

  class ModKit {
    constructor(name, filterDefaults = {}) {
      this.name = name;
      this.filterDefaults = filterDefaults;
    }

    _mod(filterWorker, processingWorkers) {
      let filteredData = filter(ctx.list, filterWorker);
      ctx.statistics.moddedByRule[this.name] = ctx.statistics.moddedByRule[this.name] || [];
      let moddedByRule = 0;
      if (processingWorkers.item) {
        filteredData.items.filter(item => processingWorkers.item(item) !== false).forEach(item => {
          addModded(ctx, item);
          moddedByRule++;
        });
      }
      if (processingWorkers.component) {
        filteredData.components.forEach(({component, item}) => {
          if (processingWorkers.component(component, item) !== false) {
            addModded(ctx, item);
            moddedByRule++;
          }
        });
      }
      ctx.statistics.moddedByRule[this.name].push(moddedByRule);
    }

    mod(type, componentType, worker) {
      if ( componentType ) {
        componentType = formatComponentName(componentType);
      }
      const filterRules = { ...this.filterDefaults };
      filterRules.type = type || filterRules.type;
      if ( componentType ) {
        filterRules.component = cmp => cmp.$type === componentType;
      }
      return this._mod(filterRules, {
        component: worker
      });
    }

    overwrite(type, componentType, data) {
      return this.mod(type, componentType, cmp => {
        Object.assign(cmp, data);
        return cmp;
      });
    }

    withDebugNames(names) {
      return new ModKit(this.name, {
        item: item => names.indexOf(item.DebugName) >= 0
      });
    }
  }

  ctx.getModKit = name => new ModKit(name);

  const sources = fs.readdirSync(rootPath())
      .filter(fileName => config.filterSourcesRe.test(fileName))
      .sort()
      .map(fileName => rootPath.bind(this, fileName));
  sources.forEach(processModule.bind(this, ctx));

  for (let type in ctx.tree) {
    const items = ctx.tree[type];
    for (let id in items) {
      ctx.list.push(items[id]);
    }
  }

  rules.forEach(r => r(ctx));

  for (let type in ctx.moddedTree) {
    const items = ctx.moddedTree[type];
    for (let id in items) {
      ctx.moddedList.push(items[id]);
    }
  }

  console.log('Modding summary per rule:\n', ctx.statistics.moddedByRule);
  console.log('Modding summary per type:\n', ctx.statistics.moddedByType);

  writeModded(ctx);

  return 0;
};

function addModded(ctx, item) {
  ctx.statistics.moddedByType[item.$type] = ctx.statistics.moddedByType[item.$type] || 0;
  ctx.statistics.moddedByType[item.$type]++;
  ctx.moddedTree[item.$type] = ctx.moddedTree[item.$type] || {};
  ctx.moddedTree[item.$type][item.ID] = item;
}

function writeModded(ctx) {
  const targetPathFile = rootPath(config.resultPath);
  createDirRec(rootPath(), path.dirname(config.resultPath));
  const newContent = JSON.stringify({GameDataObjects: ctx.moddedList}, null, 2);
  fs.writeFileSync(targetPathFile, newContent, {encoding: 'utf8'});
}

function createDirRec(basePath, targetPath) {
  if (!fs.existsSync(basePath)) {
    throw new Error('Base path not exists!');
  }
  if (targetPath.indexOf('..') >= 0) {
    throw new Error('../ not allowed!');
  }
  const targetPathMembers = targetPath.split('/');

  for (let currentPath = basePath; targetPathMembers.length;) {
    let nextPart = targetPathMembers.shift();
    currentPath = path.resolve(currentPath, nextPart);
    if (!fs.existsSync(currentPath)) {
      fs.mkdirSync(currentPath);
    }
  }

}
