module.exports = {
  filterSourcesRe: /exported$/,
  validExtensionRe: /\.gamedatabundle$/,
  bundlesPath: './design/gamedata/',
  resultPath: './override/zzzAlxOverhaulGenerated/gamedata/generated.gamedatabundle',
};
