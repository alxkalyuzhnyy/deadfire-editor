const fs = require('fs');
const processFile = require('./process-file');
const config = require('./config');

function processModule(ctx, sourcePath) {
  const bundlesPath = sourcePath.bind(this, config.bundlesPath);
  // console.log(bundlesPath());
  if (fs.existsSync(bundlesPath())) {
    const files = fs.readdirSync(bundlesPath())
        .filter(fileName => config.validExtensionRe.test(fileName))
        .sort()
        .map(fileName => bundlesPath(fileName));
    // console.log(files);
    files.forEach(processFile.bind(this, ctx));
  }
}
module.exports = processModule;
