const fs = require('fs');

function processFile(ctx, filePath) {
  let fileData;
  try {
    fileData = JSON.parse(fs.readFileSync(filePath, {encoding: 'utf8'}).trim());
  } catch (err) {
    console.warn(`Failed to parse file, skipped: ${filePath}`);
    return;
  }
  const content = fileData.GameDataObjects;
  content.forEach(record => {
    ctx.tree[record.$type] = ctx.tree[record.$type] || {};
    ctx.tree[record.$type][record.ID] = record;
  });
}

module.exports = processFile;
